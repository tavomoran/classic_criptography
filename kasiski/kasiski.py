#!/usr/bin/python3

# This script is based on the book Cracking Codes with Python http://inventwithpython.com/cracking/
# The source can be found on # https://www.nostarch.com/crackingcodes/ (BSD Licensed)
# This scripts keeps the BSD License

import argparse
import os
import os.path as path

import itertools, re
import vigenereCipher, freqAnalysis, detectEnglish

spanish_alphabet = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
english_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
SILENT_MODE = False # If set to True, program doesn't print anything.
NUM_MOST_FREQ_LETTERS = 4 # Attempt this many letters per subkey.
MAX_KEY_LENGTH = 16 # Will not attempt keys longer than this.
NONLETTERS_PATTERN = re.compile('[^A-Z]')



def findRepeatSequencesSpacings(message):
	message = NONLETTERS_PATTERN.sub('', message.upper())

	seqSpacings = {}
	for seqLen in range(3, 6):
		for seqStart in range(len(message) - seqLen):
			seq = message[seqStart:seqStart + seqLen]

			for i in range(seqStart + seqLen, len(message) - seqLen):
				if message[i:i + seqLen] == seq:
					if seq not in seqSpacings:
						seqSpacings[seq] = [] 

					seqSpacings[seq].append(i - seqStart)
	return seqSpacings


def getUsefulFactors(num):

    if num < 2:
        return [] 

    factors = [] 

    for i in range(2, MAX_KEY_LENGTH + 1): 
        if num % i == 0:
            factors.append(i)
            otherFactor = int(num / i)
            if otherFactor < MAX_KEY_LENGTH + 1 and otherFactor != 1:
                factors.append(otherFactor)
    return list(set(factors)) 


def getItemAtIndexOne(items):
	return items[1]


def getMostCommonFactors(seqFactors):
	factorCounts = {}

	for seq in seqFactors:
		factorList = seqFactors[seq]
		for factor in factorList:
			if factor not in factorCounts:
				factorCounts[factor] = 0
			factorCounts[factor] += 1

	factorsByCount = []
	for factor in factorCounts:
		if factor <= MAX_KEY_LENGTH:
			factorsByCount.append( (factor, factorCounts[factor]) )

	factorsByCount.sort(key=getItemAtIndexOne, reverse=True)

	return factorsByCount


def kasiskiExamination(ciphertext):
	repeatedSeqSpacings = findRepeatSequencesSpacings(ciphertext)

	seqFactors = {}
	for seq in repeatedSeqSpacings:
		seqFactors[seq] = []
		for spacing in repeatedSeqSpacings[seq]:
			seqFactors[seq].extend(getUsefulFactors(spacing))

	factorsByCount = getMostCommonFactors(seqFactors)

	allLikelyKeyLengths = []
	for twoIntTuple in factorsByCount:
		allLikelyKeyLengths.append(twoIntTuple[0])

	return allLikelyKeyLengths


def getNthSubkeysLetters(nth, keyLength, message):
    message = NONLETTERS_PATTERN.sub('', message)

    i = nth - 1
    letters = []
    while i < len(message):
        letters.append(message[i])
        i += keyLength
    return ''.join(letters)


def attemptHackWithKeyLength(ciphertext, mostLikelyKeyLength):
	ciphertextUp = ciphertext.upper()
	allFreqScores = []
	for nth in range(1, mostLikelyKeyLength + 1):
		nthLetters = getNthSubkeysLetters(nth, mostLikelyKeyLength, ciphertextUp)
		freqScores = []
		for possibleKey in LETTERS:
			decryptedText = vigenereCipher.decryptMessage(possibleKey, nthLetters)
			keyAndFreqMatchTuple = (possibleKey, freqAnalysis.englishFreqMatchScore(decryptedText))
			freqScores.append(keyAndFreqMatchTuple)
		freqScores.sort(key=getItemAtIndexOne, reverse=True)

		allFreqScores.append(freqScores[:NUM_MOST_FREQ_LETTERS])

	if not SILENT_MODE:
		for i in range(len(allFreqScores)):
			print('Possible letters for letter %s of the key: ' % (i + 1), end='')
			for freqScore in allFreqScores[i]:
				print('%s ' % freqScore[0], end='')
			print()

	for indexes in itertools.product(range(NUM_MOST_FREQ_LETTERS), repeat=mostLikelyKeyLength):
		possibleKey = ''
		for i in range(mostLikelyKeyLength):
			possibleKey += allFreqScores[i][indexes[i]][0]

		if not SILENT_MODE:
			print('Attempting with key: %s' % (possibleKey))

		decryptedText = vigenereCipher.decryptMessage(possibleKey, ciphertextUp)

		if detectEnglish.isEnglish(decryptedText):
			origCase = []
			for i in range(len(ciphertext)):
				if ciphertext[i].isupper():
					origCase.append(decryptedText[i].upper())
				else:
					origCase.append(decryptedText[i].lower())
			decryptedText = ''.join(origCase)

			print('Possible encryption hack with key %s:' % (possibleKey))
			print(decryptedText[:200])
			return decryptedText
	return None


def hackVigenere(ciphertext):
	allLikelyKeyLengths = kasiskiExamination(ciphertext)
	if not SILENT_MODE:
		keyLengthStr = ''
		for keyLength in allLikelyKeyLengths:
			keyLengthStr += '%s ' % (keyLength)
		print('Kasiski Examination results say the most likely key lengths are: ' + keyLengthStr + '\n')
	hackedMessage = None
	for keyLength in allLikelyKeyLengths:
		if not SILENT_MODE:
			print('Attempting hack with key length %s (%s possible keys)...' % (keyLength, NUM_MOST_FREQ_LETTERS ** keyLength))
		hackedMessage = attemptHackWithKeyLength(ciphertext, keyLength)
		if hackedMessage != None:
			break
	return hackedMessage


def main():
	alphabet = ""
	message = ""
	cryptogram = ""
	key = ""
	outputFileName = ""
	parser = argparse.ArgumentParser(description="Kasiski: Acryptanalysis technique to decipher the algorithm vigenere.  Repository https://gitlab.com/tavomoran/classic_criptography This program has not warranty, use under your own risk. Script developed by Gustavo Móran - Alix Estefany Cabrera. Class \"Introducción a la criptografia\" Teacher: Siler Amador Donado.")

	inputGroup = parser.add_mutually_exclusive_group(required = True)
	inputGroup.add_argument("-i", "--input", type=str, help="set a message to encrypt with -e flag, or a criptograma to decrypt with the -d flag")
	inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the cryptograma, use -e to encrypt or -d to decrypt"); 

	alphabetGroup = parser.add_mutually_exclusive_group()
	alphabetGroup.add_argument("-a", "--alphabet",type=str, help="set a custom alphabet")
	alphabetGroup.add_argument("-aF", "--alphabetFile", type=str, help="set a file to read an alphabet")
	alphabetGroup.add_argument("-aC", "--alphabetChoose", type=int, choices=[1,2], default=1,help="set an alphabet [1] Spanish, [2] English")
    
	outputGroup = parser.add_mutually_exclusive_group()
	outputGroup.add_argument("-o", "--output", type=str, help="set the output file name. WARNING: this will overwrite the file")
	outputGroup.add_argument("-oF", "--outputFile", action="store_true", help="set an outputFile file with the same imput FileName and .dec extension, if there is not use of the --inputFile then it will name the file default.dec ");


	args = parser.parse_args()

	if args.input:
		cryptogram = args.input;
	elif args.inputFile:
		if path.exists(args.inputFile):
			f = open(args.inputFile, encoding="ISO-8859-1")
			reading = f.read()
			f.close()
			cryptogram = reading[0:-1]
		else:
			print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))

	if args.alphabet:
		alphabet = args.alphabet
	elif args.alphabetFile:
		if path.exists(args.alphabetFile):
			f = open(args.alphabetFile, encoding="ISO-8859-1")
			reading = f.read()
			f.close()
			alphabet = reading[0:-1]
		else:
			print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
	elif args.alphabetChoose:
		if args.alphabetChoose == 1:
			alphabet = spanish_alphabet
		elif args.alphabetChoose == 2:
			alphabet = english_alphabet
		else:
			alphabet = spanish_alphabet
	else:
		alphabet = spanish_alphabet

	if args.output:
		if path.exists(args.output):
			os.remove(args.output)
		outputFileName = args.output
		message = hackVigenere(cryptogram)
		f = open(outputFileName, "x", encoding="ISO-8859-1")
		f.write(message)
		f.close()
	elif args.outputFile:
		if path.exists(args.inputFile):
			outputFileName = args.inputFile + ".dec"
			if( path.exists(outputFileName)):
				os.remove(outputFileName)
			message = hackVigenere(cryptogram)
			f = open(outputFileName, "x", encoding="ISO-8859-1")
			f.write(message)
			f.close()			
		else:
			outputFileName = "default.cif"
			if( path.exists(outputFileName)):
				os.remove(outputFileName)
			message = hackVigenere(cryptogram)
			f = open(outputFileName, "x", encoding="ISO-8859-1")
			f.write(message)
			f.close()
		message = hackVigenere(cryptogram)
	else:
		message = hackVigenere(cryptogram)
		print ("message: {}".format(message))





if __name__ == '__main__':
    main()