#!/usr/bin/python3
import argparse
import os
import os.path as path

spanish_alphabet = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
english_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def encrypt(m,k,a):
    a = a.upper()
    k = k.upper()
    m = m.upper()
    kl = []
    c = ""
    for ck in k:
        for i,ca in enumerate(a):
            if ck == ca:
                kl.append(i)
    skl = len(kl)
    sa = len(a)
    for i,cm in enumerate(m):
        for j,ca in enumerate(a):
            if cm == ca:
                c += a[(j+kl[i%skl])%sa]
    return c;

def decrypt(c,k,a):
    a = a.upper()
    k = k.upper()
    c = c.upper()
    kl = []
    m = ""
    for ck in k:
        for i,ca in enumerate(a):
            if ck == ca:
                kl.append(i)
    skl = len(kl)
    sa = len(a)
    for i,cm in enumerate(c):
        for j,ca in enumerate(a):
            if cm == ca:
                m += a[(j-kl[i%skl])%sa]
    return m;

def main():
    alphabet = ""
    message = ""
    cryptogram = ""
    key = ""
    parser = argparse.ArgumentParser(description="Vigenère cipher: A polialphabet cipher invented in 16th century by Blaise de Vigenère. Every letter of the message will be replaced. Each key letter will generate a new alphabet in which the message letters will be replaced.  Repository https://gitlab.com/tavomoran/classic_criptography This program has not warranty, use under your own risk. Script developed by Gustavo Móran - Alix Estefany Cabrera")
    inputGroup = parser.add_mutually_exclusive_group(required = True)
    inputGroup.add_argument("-i", "--input", type=str, help="set a message to encrypt with -e flag, or a criptograma to decrypt with the -d flag")
    inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the cryptograma, use -e to encrypt or -d to decrypt"); 
    keyGroup = parser.add_mutually_exclusive_group(required = True)
    keyGroup.add_argument("-k", "--key", type=str, help="set a key")
    keyGroup.add_argument("-kF", "--keyFile", type=str, help="set a file to read the key")
    alphabetGroup = parser.add_mutually_exclusive_group()
    alphabetGroup.add_argument("-a", "--alphabet",type=str, help="set a custom alphabet")
    alphabetGroup.add_argument("-aF", "--alphabetFile", type=str, help="set a file to read an alphabet")
    alphabetGroup.add_argument("-aC", "--alphabetChoose", type=int, choices=[1,2], default=1,help="set an alphabet [1] Spanish, [2] English")
    encDecGroup = parser.add_mutually_exclusive_group(required=True)
    encDecGroup.add_argument("-e", "--encrypt", action="store_true", help="to encrypt the message")
    encDecGroup.add_argument("-d","--decrypt", action="store_true", help="to decrypt the cryptogram")
    outputGroup = parser.add_mutually_exclusive_group()
    outputGroup.add_argument("-o", "--output", type=str, help="set the output file name. WARNING: this will overwrite the file")

    args = parser.parse_args()
    
    if args.key:
        key = args.key
    elif args.keyFile:
        if path.exists(args.keyFile):
            f = open(args.keyFile, encoding="ISO-8859-1")
            reading = f.read()
            f.close()
            key = reading[0:-1]
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.keyFile))
            return

    if args.alphabet:
        alphabet = args.alphabet
    elif  args.alphabetFile:
        if path.exists(args.alphabetFile):
            f = open(args.alphabetFile, encoding="ISO-8859-1")
            reading = f.read()
            f.close()
            alphabet = reading[0:-1]
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
            return
    elif args.alphabetChoose:
        if args.alphabetChoose == 1:
            alphabet = spanish_alphabet
        elif args.alphabetChoose == 2:
            alphabet = english_alphabet
        else:
            alphabet = spanish_alphabet
    else:
        alphabet = spanish_alphabet


    if args.input:
        if args.encrypt:
            message = args.input
        elif args.decrypt:
            cryptogram = args.input
    elif args.inputFile:
        reading=""
        if path.isfile(args.inputFile):
            f = open(args.inputFile, encoding="ISO-8859-1")
            reading = f.read()
            f.close()
            if args.encrypt:
                message = reading
            elif args.decrypt:
                cryptogram = reading
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
            return
        
    if args.output:
        if path.exists(args.output):
            os.remove(args.output)
        f = open (args.output, "x", encoding="ISO-8859-1")
        if args.encrypt:
            cryptogram = encrypt(message,key,alphabet)
            f.write(cryptogram)
            print ("alphabet : {}".format(alphabet))
            print ("message has been encrypted in file {}".format(args.output))
        elif args.decrypt:
            message = decrypt(cryptogram,key,alphabet)
            f.write(message)
            f.close()
            print ("alphabet : {}".format(alphabet))
            print ("cryptogram has been decrypted in file {}".format(args.output))
    else:
        if args.encrypt:
            cryptogram = encrypt(message,key,alphabet)
            print ("alphabet : {}".format(alphabet))
            print ("message : {}".format(message))
            print ("cryptogram : {}".format(cryptogram))
        elif args.decrypt:
            message = decrypt(cryptogram,key,alphabet)
            print ("alphabet : {}".format(alphabet))
            print ("cryptogram : {}".format(cryptogram))
            print ("message : {}".format(message))



if __name__ == '__main__':
    main()
