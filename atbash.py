#!/usr/bin/python3
import argparse
import os
import os.path as path

spanish_alphabet = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
english_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def encrypt(m,a):
    a = a.upper()
    m = m.upper()
    na = a[::-1]
    c = ""
    for cm in m:
        for i, ca in enumerate(a):
            if cm == ca:
                c+=na[i]
    return c;

def decrypt(c,a):
    return encrypt(c,a)
    

def main():
    alphabet = ""
    message = ""
    cryptogram = ""
    parser = argparse.ArgumentParser(description="Atbash cipher: The first cipher known in the human history. It was originally developed for use with the Hebrew aphabet. In fact the Book of Yirmeyahu (Jeremiah), there are several words that have been enciphered through the use of the Atbash Cipher. It uses a simple method, the first letter of the alphabet is replaced with the last letter, the second letter is switched with the second to last, and so on. Repository https://gitlab.com/tavomoran/classic_criptography this program has not warranty, use under your own risk. Script developed by Gustavo Morán - Alix Estefany Cabrera")
    alphabetGroup = parser.add_mutually_exclusive_group()
    alphabetGroup.add_argument("-a", "--alphabet",type=str, help="set a custom alphabet")
    alphabetGroup.add_argument("-aF", "--alphabetFile", type=str, help="set a file to read an alphabet")
    alphabetGroup.add_argument("-aC", "--alphabetChoose", type=int, choices=[1,2], default=1,help="set an alphabet [1] Spanish, [2] English")
    outputGroup = parser.add_mutually_exclusive_group()
    outputGroup.add_argument("-o", "--output", type=str, help="set the output file name")
    inputGroup = parser.add_mutually_exclusive_group(required=True)
    inputGroup.add_argument("-i", "--input", type=str, help="set a message to encrypt with -e flag, or a criptograma to decrypt with the -d flag")
    inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the cryptograma, use -e to encrypt or -d to decrypt"); 
    encDecGroup = parser.add_mutually_exclusive_group(required=True)
    encDecGroup.add_argument("-e", "--encrypt", action="store_true", help="to encrypt the message")
    encDecGroup.add_argument("-d","--decrypt", action="store_true", help="to decrypt the cryptogram")

    args = parser.parse_args()
    
    if args.alphabet:
        alphabet = args.alphabet
    elif  args.alphabetFile:
        if path.exists(args.alphabetFile):
            f = open(args.alphabetFile, encoding="ISO-8859-1")
            reading = f.read()
            alphabet = reading[0:-1]
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
            return
    elif args.alphabetChoose:
        if args.alphabetChoose == 1:
            alphabet = spanish_alphabet
        elif args.alphabetChoose == 2:
            alphabet = english_alphabet
        else:
            alphabet = spanish_alphabet
    else:
        alphabet = spanish_alphabet


    if args.input:
        if args.encrypt:
            message = args.input
        elif args.decrypt:
            cryptogram = args.input
    elif args.inputFile:
        reading=""
        if path.isfile(args.inputFile):
            f = open(args.inputFile, encoding="ISO-8859-1")
            reading = f.read()
            if args.encrypt:
                message = reading
            elif args.decrypt:
                cryptogram = reading
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
            return
        
    if args.output:
        if path.exists(args.output):
            os.remove(args.output)
        f = open(args.output, "x",encoding="ISO-8859-1")
        if args.encrypt:
            cryptogram = encrypt(message,alphabet)
            f.write(cryptogram)
            print ("alphabet : {}".format(alphabet))
            print ("message has been encrypted in file {}".format(args.output))
        elif args.decrypt:
            message = decrypt(cryptogram,alphabet)
            f.write(message)
            print ("alphabet : {}".format(alphabet))
            print ("cryptogram has been decrypted in file {}".format(args.output))
    else:
        if args.encrypt:
            cryptogram = encrypt(message,alphabet)
            print ("alphabet : {}".format(alphabet))
            print ("message: {}".format(message))
            print ("cryptogram: {}".format(cryptogram))
        elif args.decrypt:
            message = decrypt(cryptogram,alphabet)
            print ("alphabet : {}".format(alphabet))
            print ("cryptogram: {}".format(cryptogram))
            print ("message: {}".format(message))



if __name__ == '__main__':
    main()
